<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h1>Book Title</h1>
    <form>
        <div class="form-group">
            <label for="name">Book Name:</label>
            <input type="name" class="form-control" id="email" placeholder="Enter the book name">
        </div>


        <button type="submit" class="btn badge">Submit</button>
        <button type="Add" class="btn badge">Add</button>
        <button type="Save" class="btn badge">Save</button>
        <button type="RESET" class="btn badge">Reset</button>
        <button type="Download" class="btn badge">Download PDF</button>
    </form>
</div>

</body>
</html>